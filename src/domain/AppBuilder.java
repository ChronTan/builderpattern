package domain;

public class AppBuilder {
    public static void main(String[] args) {
        Car car=new CarBuilder()
                .buildMake("BMW")
                .buildTransmission(Transmission.AUTO)
                .buildMaxSpeed(200)
                .build();
        System.out.println(car);
    }
}
