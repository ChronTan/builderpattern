package domain;

public enum Transmission {
    AUTO, MANUAL;
}
